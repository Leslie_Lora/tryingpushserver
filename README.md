# ChineseLiteratureApp
Yuting Jing 20086428. </br></br>
We can add and delete users, authors, poems and poems to their authors' works list. Users can register, login, logout, give thumbs up and take back the thumbs up to poems and author. We can also find poems by poemId, find authors by authorId, find users by userId. In login function, I also made a session-based authentication which mainly learned from this link: https://medium.com/createdd-notes/starting-with-authentication-a-tutorial-with-node-js-and-mongodb-25d524ca0359. </br></br>
I use MongoDB Atlas to store my data, use GitHub to store my files, and use Postman to do some basic automated testing.</br></br>
Github link: https://github.com/Yayooooooo/ChinesePoetryApp</br></br>
